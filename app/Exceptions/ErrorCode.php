<?php
/**
 * Created by PhpStorm.
 * User: 王越
 * Date: 2018/3/30
 * Time: 11:29
 */

namespace App\Exceptions;


/**
 *
 * 1 验证 2 逻辑错误 3 表单 4 请求资源 5 数据库错误
 * 0 通用 1 支付问题
 * 0
 * 0
 * 1 用户 2 房源 3 医生 4医院
 *
 * 10001 用户验证错误
 */
class ErrorCode
{

    const DOCTOR_NOT_FOUND = 40003;
    const HOSPITAL_NOT_FOUND = 40004;

}