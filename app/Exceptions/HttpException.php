<?php
/**
 * Created by PhpStorm.
 * User: 王越
 * Date: 2018/3/30
 * Time: 11:19
 */

namespace App\Exceptions;

use Pregnotice\Api\Exceptions\HttpException as BaseException;

/**
 * Class HttpException
 * @package App\Exceptions
 */
class HttpException extends BaseException
{
    /**
     * 鉴权相关1开头
     * 数据验证相关3开头
     * 资源相关4开头
     * 逻辑相关5开头
     *
     */
    const MESSAGE_LIST = [
        ErrorCode::DOCTOR_NOT_FOUND   => '医生不存在',
        ErrorCode::HOSPITAL_NOT_FOUND => '医院不存在'
    ];
}