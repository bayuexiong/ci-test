<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use Pregnotice\Api\Http\Helpers;

class Controller extends BaseController
{
    use Helpers;
}
