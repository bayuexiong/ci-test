<?php
/**
 * Created by PhpStorm.
 * User: 王越
 * Date: 2018/6/21
 * Time: 14:07
 */

namespace App\Http\Controllers;


use App\Exceptions\ErrorCode;
use App\Exceptions\HttpException;
use App\Models\Doctors\Doctor;
use App\Resources\Doctors\DoctorResource;

class DoctorController extends Controller
{
    /**
     * @param $doctor_id
     * @param Doctor $doctor
     * @return \Illuminate\Http\Response
     * @throws HttpException
     */
    public function doctor($doctor_id, Doctor $doctor)
    {
        $doctor = $doctor->find($doctor_id);
        if ($doctor) {
            return $this->response->json(new DoctorResource($doctor));
        }
        throw new HttpException(ErrorCode::DOCTOR_NOT_FOUND);
    }

    public function doctorList()
    {

    }

}