<?php
/**
 * Created by PhpStorm.
 * User: 王越
 * Date: 2018/6/21
 * Time: 14:22
 */

namespace App\Models\Doctors;


use App\Models\Model;

class Doctor extends Model
{
    protected $table = 'doctors';
    public $timestamps = true;

}