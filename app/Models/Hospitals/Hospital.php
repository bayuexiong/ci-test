<?php
/**
 * Created by PhpStorm.
 * User: 王越
 * Date: 2018/6/21
 * Time: 14:23
 */

namespace App\Models\Hospitals;


use App\Models\Model;

class Hospital extends Model
{
    public $timestamps = true;
    protected $table = 'hospitals';

}