<?php
/**
 * Created by PhpStorm.
 * User: 王越
 * Date: 2018/6/21
 * Time: 14:06
 */

namespace App\Models;


class Model extends \Illuminate\Database\Eloquent\Model
{
    const UPDATED_AT = 'updated';
    const CREATED_AT = 'created';
    const ACTIVE = 1;
    const DELETE = 0;
}