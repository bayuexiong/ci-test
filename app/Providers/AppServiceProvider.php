<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Jenssegers\Date\Date;
use App\Resources\Resource;
use Faker\Generator;
use Faker\Factory as Faker;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        Date::setLocale('zh-CN');
        Resource::withoutWrapping();
        $this->app->singleton(Generator::class, function () {
            return Faker::create('zh_CN');
        });
    }
}
