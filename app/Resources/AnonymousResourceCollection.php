<?php
/**
 * Created by PhpStorm.
 * User: 王越
 * Date: 2018/4/3
 * Time: 13:31
 */

namespace App\Resources;


class AnonymousResourceCollection extends ResourceCollection
{
    /**
     * The name of the resource being collected.
     *
     * @var string
     */
    public $collects;

    /**
     * Create a new anonymous resource collection.
     *
     * @param  mixed $resource
     * @param  string $collects
     * @return void
     */
    public function __construct($resource, $collects)
    {
        $this->collects = $collects;

        parent::__construct($resource);
    }

}