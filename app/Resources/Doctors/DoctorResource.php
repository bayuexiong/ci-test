<?php
/**
 * Created by PhpStorm.
 * User: 王越
 * Date: 2018/6/21
 * Time: 14:24
 */

namespace App\Resources\Doctors;


use App\Resources\Resource;

class DoctorResource extends Resource
{

    public function toArray($request)
    {
        return [
            'id' => $this->id
        ];
    }
}