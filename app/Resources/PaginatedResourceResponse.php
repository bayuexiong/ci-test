<?php
/**
 * Created by PhpStorm.
 * User: 王越
 * Date: 2018/4/3
 * Time: 14:46
 */

namespace App\Resources;

use Illuminate\Support\Arr;
use Pregnotice\Api\Resources\Json\PaginatedResourceResponse as BaseResponse;

class PaginatedResourceResponse extends BaseResponse
{
    /**
     * @inheritDoc
     */
    protected function paginationInformation($request)
    {
        $paginated = $this->resource->resource->toArray();
        return $this->meta($paginated);
    }

    /**
     * @inheritDoc
     */
    protected function meta($paginated)
    {
        $hasMore = Arr::get($paginated, 'current_page') == Arr::get($paginated, 'last_page') ? false : true;
        if (Arr::get($paginated, 'total') == 0) {
            $hasMore = false;
        }
        return compact('hasMore');
    }


}