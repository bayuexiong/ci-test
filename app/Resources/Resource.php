<?php
/**
 * Created by PhpStorm.
 * User: 王越
 * Date: 2018/3/23
 * Time: 16:07
 */

namespace App\Resources;



class Resource extends \Pregnotice\Api\Resources\Json\Resource
{
    /**
     * @inheritDoc
     */
    public static function collection($resource)
    {
        return new AnonymousResourceCollection($resource, get_called_class());
    }
}