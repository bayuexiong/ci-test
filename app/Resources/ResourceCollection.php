<?php
/**
 * Created by PhpStorm.
 * User: 王越
 * Date: 2018/4/3
 * Time: 13:32
 */

namespace App\Resources;

use Illuminate\Pagination\AbstractPaginator;
use Pregnotice\Api\Resources\Json\ResourceCollection as BaseCollection;

class ResourceCollection extends BaseCollection
{
    public static $wrap = 'items';
    /**
     * @inheritDoc
     */
    public function toResponse($request)
    {
        return $this->resource instanceof AbstractPaginator
            ? (new PaginatedResourceResponse($this))->toResponse($request)
            : parent::toResponse($request);
    }

    public function toResource($request)
    {
        return $this->resource instanceof AbstractPaginator
            ? (new PaginatedResourceResponse($this))->toResource($request)
            : parent::toResource($request);
    }


}