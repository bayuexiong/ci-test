<?php


!defined('APP_PATH') && define('APP_PATH', dirname(__DIR__));
require_once APP_PATH . '/vendor/autoload.php';

try {
    (new Dotenv\Dotenv(APP_PATH))->load();
} catch (Dotenv\Exception\InvalidPathException $e) {
    //
}

/*
|--------------------------------------------------------------------------
| Create The Application
|--------------------------------------------------------------------------
|
| Here we will load the environment and create the application instance
| that serves as the central piece of this framework. We'll use this
| application as an "IoC" container and router for this framework.
|
*/

$app = new Laravel\Lumen\Application(
    realpath(APP_PATH)
);

$app->withFacades();

$app->withEloquent();

$app->configure('app');
$app->configure('api_log');
$app->configure('cors');
$app->configure('database');

/*
|--------------------------------------------------------------------------
| Register Container Bindings
|--------------------------------------------------------------------------
|
| Now we will register a few bindings in the service container. We will
| register the exception handler and the console kernel. You may add
| your own bindings here if you like or you can make another file.
|
*/

$app->singleton(
    Illuminate\Contracts\Debug\ExceptionHandler::class,
    App\Exceptions\Handler::class
);

$app->singleton(
    Illuminate\Contracts\Console\Kernel::class,
    App\Console\Kernel::class
);

/*
|--------------------------------------------------------------------------
| Register Middleware
|--------------------------------------------------------------------------
|
| Next, we will register the middleware with the application. These can
| be global middleware that run before and after each request into a
| route or middleware that'll be assigned to some specific routes.
|
*/

$app->middleware([
    Barryvdh\Cors\HandleCors::class,
    Pregnotice\Api\Middleware\RepeatRequest::class,
]);

$app->routeMiddleware([
//    'jwt.auth' => App\Http\Middleware\JWTMiddleware::class,
]);

/*
|--------------------------------------------------------------------------
| Register Service Providers
|--------------------------------------------------------------------------
|
| Here we will register all of the application's service providers which
| are used to bind services into the container. Service providers are
| totally optional, so you are not required to uncomment this line.
|
*/

$app->register(App\Providers\AppServiceProvider::class);
$app->register(App\Providers\EventServiceProvider::class);
$app->register(Illuminate\Redis\RedisServiceProvider::class);

$app->register(Pregnotice\Api\Providers\ApiServiceProvider::class);
$app->register(Pregnotice\Api\Providers\LumenLoggerChannelServiceProvider::class);
$app->register(Barryvdh\Cors\ServiceProvider::class);

if ($app->environment('local')) {
    $app->configure('ide-helper');
    $app->register('Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider');
    $app->register('Clockwork\Support\Lumen\ClockworkServiceProvider');
}
/*
|--------------------------------------------------------------------------
| Load The Application Routes
|--------------------------------------------------------------------------
|
| Next we will include the routes file so that they can all be added to
| the application. This will provide all of the URLs the application
| can respond to, as well as the controllers that may handle them.
|
*/

$app->group(['namespace' => 'App\Http\Controllers'], function ($app) {
    require APP_PATH . '/routes/web.php';
});

return $app;
