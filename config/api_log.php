<?php
/**
 * Created by PhpStorm.
 * User: 王越
 * Date: 2018/3/26
 * Time: 18:38
 */

use Monolog\Handler\RotatingFileHandler;

return [
    'channels'        => [
        'default'
    ],
    'cli_mode'        => true,
    'default_channel' => env('LOG_DEFAULT', 'default'),
    'default'         => [
        'levels'   => ['info', 'error'],
        'path'     => env('LOG_PATH', storage_path('logs/')),
        'cli_path' => env('LOG_CLI_PATH', storage_path('cli_logs/')),
        'handler'  => RotatingFileHandler::class,
        'day'      => 7
    ],
    'sys'             => [
        'levels'   => 'error',
        'path'     => env('LOG_PATH', storage_path('logs/')),
        'cli_path' => env('LOG_CLI_PATH', storage_path('cli_logs/')),
        'handler'  => RotatingFileHandler::class,
        'day'      => 7
    ]
];