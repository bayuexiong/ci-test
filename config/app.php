<?php
/**
 * Created by PhpStorm.
 * User: 王越
 * Date: 2018/5/27
 * Time: 15:54
 */
return [
    'server_base'  => env('SERVER_BASE', 'http://composer.ladybirdedu.com/question/index.php/'),
    'search'       => [
    ],
    'static'       => [
        'store_dir'   => env('STATIC_STORE_DIR', ''),
        'public_url'  => env('STATIC_PUBLIC_URL', ''),
        'public_host' => env('STATIC_PUBLIC_HOST', ''),
    ]
];