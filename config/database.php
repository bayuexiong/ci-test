<?php
/**
 * Created by PhpStorm.
 * User: 王越
 * Date: 2017/3/23
 * Time: 17:24
 */
return [
    'default'     => env('DB_DEFAULT', 'default'),
    'migrations'  => 'migrations',
    'connections' => [
        'default' => [
            'driver'    => 'mysql',
            'host'      => env('DB_HOST', 'localhost'),
            'port'      => env('DB_PORT', '3306'),
            'database'  => env('DB_DATABASE', 'accounts'),
            'username'  => env('DB_USERNAME', 'root'),
            'password'  => env('DB_PASSWORD', 'secret'),
            'charset'   => 'utf8mb4',
            'collation' => 'utf8mb4_general_ci',
            'prefix'    => '',
            'strict'    => false,
        ],
        'salve'   => [
            'driver'    => 'mysql',
            'host'      => env('DB_HOST', 'localhost'),
            'port'      => env('DB_PORT', '3306'),
            'database'  => env('DB_DATABASE', 'accounts'),
            'username'  => env('DB_USERNAME', 'root'),
            'password'  => env('DB_PASSWORD', 'secret'),
            'charset'   => 'utf8mb4',
            'collation' => 'utf8mb4_general_ci',
            'prefix'    => '',
            'strict'    => false,
        ],
        'local'   => [
            'driver'    => 'mysql',
            'host'      => env('DB_LOCAL_HOST'),
            'database'  => env('DB_LOCAL_DATABASE'),
            'username'  => env('DB_LOCAL_USERNAME'),
            'password'  => env('DB_LOCAL_PASSWORD'),
            'port'      => env('DB_LOCAL_PORT'),
            'charset'   => 'utf8mb4',
            'collation' => 'utf8mb4_general_ci',
            'prefix'    => '',
            'strict'    => false,
        ],
        'ios'     => [
            'driver'    => 'mysql',
            'host'      => env('DB_IOS_HOST', 'localhost'),
            'port'      => env('DB_IOS_PORT', '3306'),
            'database'  => env('DB_IOS_DATABASE', 'accounts'),
            'username'  => env('DB_IOS_USERNAME', 'root'),
            'password'  => env('DB_IOS_PASSWORD', 'secret'),
            'charset'   => 'utf8',
            'collation' => 'utf8_general_ci',
            'prefix'    => '',
            'strict'    => false,
        ],
        'develop' => [
            'driver'    => 'mysql',
            'host'      => env('DB_DEVELOP_HOST', 'localhost'),
            'port'      => env('DB_DEVELOP_PORT', '3306'),
            'database'  => env('DB_DEVELOP_DATABASE', 'develop'),
            'username'  => env('DB_DEVELOP_USERNAME', 'root'),
            'password'  => env('DB_DEVELOP_PASSWORD', 'secret'),
            'charset'   => 'utf8',
            'collation' => 'utf8_general_ci',
            'prefix'    => '',
            'strict'    => false,
        ],
    ],

    'redis' => [

        'cluster' => false,

        'default' => [
            'host'     => env('REDIS_HOST', 'localhost'),
            'password' => env('REDIS_PASSWORD', null),
            'port'     => env('REDIS_PORT', 6379),
            'prefix'   => env('REDIS_PREFIX', 'laravel'),
            'database' => 0,
        ],

        'session' => [
            'host'     => env('REDIS_HOST', 'localhost'),
            'password' => env('REDIS_PASSWORD', null),
            'port'     => env('REDIS_PORT', 6379),
            'prefix'   => env('REDIS_PREFIX', 'laravel'),
            'database' => 1,
        ],
        'local'   => [
            'host'     => env('REDIS_HOST', 'localhost'),
            'password' => env('REDIS_PASSWORD', null),
            'port'     => env('REDIS_PORT', 6379),
            'prefix'   => env('REDIS_PREFIX', 'laravel'),
            'database' => 0,
        ]
    ],
];
