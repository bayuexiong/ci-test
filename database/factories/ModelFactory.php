<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/


$factory->define(App\Models\Doctors\Doctor::class, function (Faker\Generator $faker) {
    return [
        'name_cn'    => $faker->name(),
        'name_en'    => 'cc',
        'title'      => '主治医师',
        'address_cn' => $faker->address,
        'address_en' => 'gu yang building 1408',
        'tel'        => '05213218033806',
        'race'       => '华人医生',
        'exp_year'   => rand(1, 20),
        'lang'       => '英语，中文',
        'introduce'  => $faker->text,
        'fax'        => '05213218033806',
        'email'      => 'wangyue@corp-ci.com',
        'work_time'  => '周一到周六早上10点到12点，下午13点到16点',
        'rate'       => rand(1, 5),
        'rate_num'   => rand(100, 1000),
        'state'      => 1
    ];
});

$factory->define(App\Models\Hospitals\Hospital::class, function (Faker\Generator $faker) {
    return [
        'name_cn'    => $faker->name(),
        'name_en'    => 'cc',
        'nicu'       => rand(1, 3),
        'address_cn' => $faker->address,
        'address_en' => 'gu yang building 1408',
        'open_hour'  => '周一到周六早上10点到12点，下午13点到16点',
        'tel'        => '05213218033806',
        'bed_num'    => rand(10, 40),
        'introduce'  => $faker->text,
        'longitude'  => rand(20, 50),
        'latitude'   => rand(20, 50),
        'rate'       => rand(1, 5),
        'rate_num'   => rand(100, 1000),
        'state'      => 1
    ];
});
