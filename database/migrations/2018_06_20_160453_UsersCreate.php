<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UsersCreate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nickname', 20);
            $table->string('avatar', 255);
            $table->string('country', 50);
            $table->string('province', 50);
            $table->string('city', 50);
            $table->string('phone', 20);
            $table->string('open_id', 128);
            $table->string('union_id', 128);
            $table->dateTime('updated');
            $table->dateTime('created');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
