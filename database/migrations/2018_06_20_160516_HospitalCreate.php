<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class HospitalCreate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hospitals', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name_cn', 50);
            $table->string('name_en', 50);
            $table->tinyInteger('nicu')->comment('nicu的等级 1-3');
            $table->string('address_cn', 255)->comment('中文地址');
            $table->string('address_en', 255)->comment('英文地址');
            $table->string('tel', 20)->comment('联系电话');
            $table->integer('bed_num')->comment('床位');
            $table->string('introduce', 255)->comment('介绍');
            $table->string('open_hour', 100)->comment('工作时间, e.g 周一到周六 早上');
            $table->decimal('longitude')->comment('经度');
            $table->decimal('latitude')->comment('纬度');
            $table->float('rate')->comment('评分，保留2位小数');
            $table->integer('rate_num')->comment('评分数量');
            $table->tinyInteger('state')->comment('1-在线 0-不在线');
            $table->dateTime('updated');
            $table->dateTime('created');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('hospitals');
    }
}
