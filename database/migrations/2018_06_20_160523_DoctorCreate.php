<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DoctorCreate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('doctors', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name_cn', 50);
            $table->string('name_en', 50);
            $table->string('title', 20)->comment('职称');
            $table->string('address_cn', 255)->comment('中文地址');
            $table->string('address_en', 255)->comment('中文地址');
            $table->string('tel', 20)->comment('联系电话');
            $table->string('race', 20)->comment('医生人种 e.g. 白人医生');
            $table->integer('exp_year')->comment('工作时间');
            $table->string('lang', 100)->comment('擅长语言');
            $table->string('introduce', 255)->comment('介绍');
            $table->string('fax', 20)->comment('传真号');
            $table->string('email', 50)->comment('电子邮件');
            $table->string('work_time', 100)->comment('工作时间, e.g 周一到周六 早上');
            $table->float('rate')->comment('评分，保留2位小数');
            $table->integer('rate_num')->comment('评分数量');
            $table->tinyInteger('state')->comment('1-在线 0-不在线');
            $table->dateTime('updated');
            $table->dateTime('created');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('doctors');
    }
}
