<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DoctorRateCreate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('doctor_rate', function (Blueprint $table) {
            $table->increments('id');
            $table->tinyInteger('type')->comment('');
            $table->integer('doctor_id');
            $table->string('name', 50);
            $table->integer('full_score')->comment('评分总分');
            $table->decimal('score')->comment('得分');
            $table->dateTime('created');
            $table->index('doctor_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('doctor_rate');
    }
}
