<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DoctorResumeCreate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('doctor_resume', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('doctor_id');
            $table->string('name', 50)->comment('保险项目');
            $table->dateTime('created');
            $table->index('doctor_id');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('doctor_rate');
    }
}
