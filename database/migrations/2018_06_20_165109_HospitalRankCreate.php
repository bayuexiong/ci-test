<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class HospitalRankCreate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hospital_rank', function (Blueprint $table) {
            $table->increments('id');
            $table->tinyInteger('rank_type')->comment('排名类型');
            $table->integer('hospital_id');
            $table->integer('rank')->comment('排名');
            $table->dateTime('created');
            $table->index('hospital_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('hospital_rank');
    }
}
