<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class HospitalChargeCreate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hospital_charge', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('hospital_id');
            $table->integer('type')->comment('收费项目类型');
            $table->string('name', 50)->comment('收费项目名称');
            $table->decimal('price')->comment('单位元');
            $table->tinyInteger('price_type')->comment('1. price * days ，价格是每天 2. price ,价格是总价');
            $table->integer('days')->comment('多少天');
            $table->dateTime('created');
            $table->index('hospital_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('hospital_charge');
    }
}
