<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$app->get('/', function () use ($app) {
    return $app->version();
});

$app->group(['prefix' => 'doctor'], function ($app) {
    $app->get('/list', 'DoctorController@doctorList');
    $app->get('/{doctor_id}', 'DoctorController@doctor');
});

$app->group(['prefix' => 'hospital'], function ($app) {
    $app->get('/list', 'HospitalController@hospitalList');
    $app->get('/{hospital_id}', 'HospitalController@hospital');
});
