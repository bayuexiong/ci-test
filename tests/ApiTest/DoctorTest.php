<?php
/**
 * Created by PhpStorm.
 * User: 王越
 * Date: 2018/6/21
 * Time: 15:15
 */

class DoctorTest extends TestCase
{
    public function testDoctorDetailSuccess()
    {
        $this->call('GET', '/doctor/1');
        $this->assertResponseOk();
        $this->assertJson(json_encode([
            'status' => 'success',
            'code'   => 1,
            'data'   => [
                'id' => 1
            ]
        ]));
    }

    public function testDoctorDetailFailure()
    {
        $this->call('GET', '/doctor/100');
        $this->assertResponseStatus(500);
    }

}